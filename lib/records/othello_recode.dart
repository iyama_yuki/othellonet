import 'package:cloud_firestore/cloud_firestore.dart';

/// Firestoreから取得した盤面データを記録するクラス
class OthelloRecorde {
  final String history;
  final DocumentReference reference;

  OthelloRecorde.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['history'] != null),
        history = map['history'];

  /// スナップショットから各変数に記録
  OthelloRecorde.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data, reference: snapshot.reference);
}
