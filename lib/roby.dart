import 'package:flutter/material.dart';
import 'package:othello_net/playroom.dart';

/// 待機用のロビーを表示するウィジェット
class Roby extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: Text('Roby')), body: Column(children: <Widget>[header, buildBody(context)]));
  }
}

Widget header = Center(child: Text("対局ルーム"));
Widget buildBody(BuildContext context) {
  return Container(
      child: Column(
    children: <Widget>[roomA(context), roomB, roomC],
  ));
}

Widget roomA(context) {
  return Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
    Text("ROOM＃A"),

    /// ボタンのデザインは以下をまるパク。。参考にした
    /// https://qiita.com/coka__01/items/30716f42e4a909334c9f
    RaisedButton(
      child: Text("BLACK"),
      color: Colors.white,
      shape: Border(
        top: BorderSide(color: Colors.red),
        left: BorderSide(color: Colors.blue),
        right: BorderSide(color: Colors.yellow),
        bottom: BorderSide(color: Colors.green),
      ),
      onPressed: () {
        Navigator.push(
            context,
            new MaterialPageRoute<Null>(
                settings: const RouteSettings(name: "/roby"), builder: (BuildContext context) => PlayRoom(2)));
      },
    ),
    RaisedButton(
      child: Text("WHITE"),
      color: Colors.white,
      shape: Border(
        top: BorderSide(color: Colors.red),
        left: BorderSide(color: Colors.blue),
        right: BorderSide(color: Colors.yellow),
        bottom: BorderSide(color: Colors.green),
      ),
      onPressed: () {
        Navigator.push(
            context,
            new MaterialPageRoute<Null>(
                settings: const RouteSettings(name: "/roby"), builder: (BuildContext context) => PlayRoom(1)));
      },
    ),
  ]);
}

Widget roomB = Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
  Text("ROOM＃B"),
  RaisedButton(
    child: Text("BLACK"),
    color: Colors.white,
    shape: Border(
      top: BorderSide(color: Colors.red),
      left: BorderSide(color: Colors.blue),
      right: BorderSide(color: Colors.yellow),
      bottom: BorderSide(color: Colors.green),
    ),
    onPressed: () {},
  ),
  RaisedButton(
    child: Text("WHITE"),
    color: Colors.white,
    shape: Border(
      top: BorderSide(color: Colors.red),
      left: BorderSide(color: Colors.blue),
      right: BorderSide(color: Colors.yellow),
      bottom: BorderSide(color: Colors.green),
    ),
    onPressed: () {},
  ),
]);
Widget roomC = Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
  Text("ROOM＃C"),
  RaisedButton(
    child: Text("BLACK"),
    color: Colors.white,
    shape: Border(
      top: BorderSide(color: Colors.red),
      left: BorderSide(color: Colors.blue),
      right: BorderSide(color: Colors.yellow),
      bottom: BorderSide(color: Colors.green),
    ),
    onPressed: () {},
  ),
  RaisedButton(
    child: Text("WHITE"),
    color: Colors.white,
    shape: Border(
      top: BorderSide(color: Colors.red),
      left: BorderSide(color: Colors.blue),
      right: BorderSide(color: Colors.yellow),
      bottom: BorderSide(color: Colors.green),
    ),
    onPressed: () {},
  ),
]);
