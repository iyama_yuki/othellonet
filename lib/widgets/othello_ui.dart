import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:othello_net/logics/othello_logic.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:othello_net/records/othello_recode.dart';

import '../result.dart';

/// オセロ盤のウィジェット
class OthelloWidget extends StatefulWidget {
  int myself;
  bool postResultPage = false;
  @override
  _OthelloWidgetState createState() {
    return _OthelloWidgetState(myself);
  }

  //OthelloWidget({Key key, this.postResultPage}) : super(key: key);

  /// ロビーで選択した石の色をロジック部に送る
  OthelloWidget(int myself) {
    this.myself = myself;
  }
}

/// オセロ盤のタップイベントを受け取る
class _OthelloWidgetState extends State<OthelloWidget> {
  OthelloLogic logic;
  OthelloRecorde recode;
  List<List> board;

  /// 相手番の時にタップイベントを無効化する
  bool isTapEnabled;

  _OthelloWidgetState(int myself) {
    logic = new OthelloLogic(myself);
  }
  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(OthelloWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    black = logic.countBlack();
//    if (postResultPage) {
//      Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context) => new Result(black, white)));
//      Navigator.of(context).push( MaterialPageRoute(builder: (BuildContext context) => new Result(black, white)));
//    }
  }

  /// 盤面をタップされた時のイベントを受け取る
  void _tapBoard(PointerEvent details, int columnIndex, int rowIndex) {
    setState(() {
      /// 盤面を更新
      if (logic.updateBoard(columnIndex, rowIndex)) {
        /// 盤面に変更があった場合は、Firestoreの記譜を更新
        recode.reference.updateData({'history': logic.getBoardHistory});
      }

      /// パスを判定
      if (logic.checkPass()) {
        logic.changeTurn();

        /// 二人ともパスなら対局終了
        if (logic.checkPass()) {
          /// 試合結果を判定
          final int black = logic.countBlack();
          final int white = 64 - black;
          //postResultPage = true;
          /*Navigator.push(
              context,
              new MaterialPageRoute<Null>(
                  settings: const RouteSettings(name: "/othelloui"),
                  builder: (BuildContext context) => Result(black, white)));*/
          /*if (black > 32) {
            msg = black.toString() + "対" + white.toString() + "で黒の勝ちです。";
          } else if (black < 32) {
            msg = white.toString() + "対" + black.toString() + "で白の勝ちです。";
          } else {
            msg = "引き分けです。";
          }*/
        } else {
          /// ユーザに通知
          /// https://pub.dev/packages/fluttertoast
          /// 打つ場所がなければパスを通知
          /// どちらも打てなければ、勝敗を通知
          Fluttertoast.showToast(
              msg: "Pass",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.white,
              textColor: Colors.black,
              fontSize: 16.0);
        }
      }
    });
  }

  int black;
  int white;

//  void postPage() {
//    if (postResultPage) {
//      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => new Result(black, white)));
//      postResultPage = false;
//    }
//  }

  /// オセロ盤に1行X8列のマスを８行分表示
  @override
  Widget build(BuildContext context) {
    this.isTapEnabled = true;

    /// Firestoreから記譜を取得
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('rooms').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();

        /// firestoreから取得したデータを記録
        recode = OthelloRecorde.fromSnapshot(snapshot.data.documents[0]);

        /// 取得した記譜データから盤面を生成
        if (logic.createBoardFromHistory(recode.history) == false) {
          black = logic.countBlack();
          white = 64 - black;
          widget.postResultPage = true;
          if (widget.myself == 1) {
            widget.myself = 2;
          } else {
            widget.myself = 1;
          }

          //WidgetsBinding.instance.addPostFrameCallback((_) => postPage());
        }

        /// 現在の盤面を取得
        board = logic.getCurrentBoard;

        return drawVertical();
      },
    );
  }

  /// 水力方向のマスを表示
  Widget drawVertical() {
    return Container(
      margin: const EdgeInsets.all(10.0),
      color: Colors.black,
      width: 320,
      height: 320,
      child: Column(children: <Widget>[
        Expanded(
          child: Container(child: drawHorizontal(1)),
        ),
        Expanded(
          child: Container(child: drawHorizontal(2)),
        ),
        Expanded(
          child: Container(child: drawHorizontal(3)),
        ),
        Expanded(
          child: Container(child: drawHorizontal(4)),
        ),
        Expanded(
          child: Container(child: drawHorizontal(5)),
        ),
        Expanded(
          child: Container(child: drawHorizontal(6)),
        ),
        Expanded(
          child: Container(child: drawHorizontal(7)),
        ),
        Expanded(
          child: Container(child: drawHorizontal(8)),
        ),
      ]),
    );
  }

  /// 水平方向のマスを表示
  Widget drawHorizontal(int columnIndex) {
    return Row(children: <Widget>[
      Expanded(
          child: this.isTapEnabled
              ? Listener(
                  onPointerUp: (PointerEvent event) => _tapBoard(event, columnIndex, 1),
                  child: colorDitector(columnIndex, 1))
              : colorDitector(columnIndex, 1)),
      Expanded(
          child: this.isTapEnabled
              ? Listener(
                  onPointerUp: (PointerEvent event) => _tapBoard(event, columnIndex, 2),
                  child: colorDitector(columnIndex, 2))
              : colorDitector(columnIndex, 2)),
      Expanded(
          child: this.isTapEnabled
              ? Listener(
                  onPointerUp: (PointerEvent event) => _tapBoard(event, columnIndex, 3),
                  child: colorDitector(columnIndex, 3))
              : colorDitector(columnIndex, 3)),
      Expanded(
          child: this.isTapEnabled
              ? Listener(
                  onPointerUp: (PointerEvent event) => _tapBoard(event, columnIndex, 4),
                  child: colorDitector(columnIndex, 4))
              : colorDitector(columnIndex, 4)),
      Expanded(
          child: this.isTapEnabled
              ? Listener(
                  onPointerUp: (PointerEvent event) => _tapBoard(event, columnIndex, 5),
                  child: colorDitector(columnIndex, 5))
              : colorDitector(columnIndex, 5)),
      Expanded(
          child: this.isTapEnabled
              ? Listener(
                  onPointerUp: (PointerEvent event) => _tapBoard(event, columnIndex, 6),
                  child: colorDitector(columnIndex, 6))
              : colorDitector(columnIndex, 6)),
      Expanded(
          child: this.isTapEnabled
              ? Listener(
                  onPointerUp: (PointerEvent event) => _tapBoard(event, columnIndex, 7),
                  child: colorDitector(columnIndex, 7))
              : colorDitector(columnIndex, 7)),
      Expanded(
          child: this.isTapEnabled
              ? Listener(
                  onPointerUp: (PointerEvent event) => _tapBoard(event, columnIndex, 8),
                  child: colorDitector(columnIndex, 8))
              : colorDitector(columnIndex, 8)),
    ]);
  }

  /// マスの色を決定
  Widget colorDitector(int columnIndex, int rowIndex) {
    if (board[columnIndex][rowIndex] == 1) {
      return Container(
          margin: EdgeInsets.all(1.0),
          color: Colors.green,
          child: Container(
            margin: EdgeInsets.all(1.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Colors.white,
            ),
          ));
    } else if (board[columnIndex][rowIndex] == 2) {
      return Container(
          margin: EdgeInsets.all(1.0),
          color: Colors.green,
          child: Container(
            margin: EdgeInsets.all(1.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Colors.black,
            ),
          ));
    } else if (board[columnIndex][rowIndex] == 3) {
      return Container(margin: EdgeInsets.all(1.0), color: Colors.grey);
    } else {
      return Container(margin: EdgeInsets.all(1.0), color: Colors.green);
    }
  }
}
