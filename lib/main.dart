import 'package:flutter/material.dart';
import 'package:othello_net/playroom.dart';
import 'package:othello_net/roby.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Othello',
      home: Roby(),
    );
  }
}
