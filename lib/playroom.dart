import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:othello_net/widgets/othello_ui.dart';

/// 対戦部屋を表示するウィジェット
class PlayRoom extends StatelessWidget {
  int myself;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('ROOM ＃A')),
        body: Column(children: <Widget>[
          /// 相手の情報を表示するセクション
          opponentInfoSection,

          /// 盤面を表示するセクション
          buildBody(myself),

          /// 自分の情報を表示するセクション
          myselfInfoSection
        ]));
  }

  /// ロビーで選択した石の色をロジック部に送る
  PlayRoom(int myself) {
    this.myself = myself;
  }

  /// 相手の情報を表示
  Widget opponentInfoSection = Container(
      height: 40.0,
      padding: EdgeInsets.only(left: 26, right: 26),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        textDirection: TextDirection.rtl,
        children: [
          Text(
            "PLAYER2 : USER NAME",
            style: new TextStyle(
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ],
      ));

  /// 盤面を表示
  Widget buildBody(int myself) {
    return Center(child: OthelloWidget(myself));
  }

  /// 自分の情報を表示
  Widget myselfInfoSection = Container(
      height: 40.0,
      padding: EdgeInsets.only(left: 26, right: 26),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.ltr,
        children: [
          Text(
            "PLAYER1 : USER NAME",
            style: new TextStyle(
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ],
      ));
}
