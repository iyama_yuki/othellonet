/// オセロのロジックを担当するクラス
class OthelloLogic {
  /// オセロ盤の状態を管理する配列
  /// 1:白石、2:黒石、0:空白、-1:壁、３:着手可能
  List<List> board;

  /// 現在のターンを記録
  /// １なら白番、２なら黒番
  int turn;

  /// 次のターンを記録
  /// １なら白番、２なら黒番
  int nextTurn;

  /// 自身の色
  /// １なら白番、２なら黒番
  int myself;

  /// 相手の色
  /// １なら白番、２なら黒番
  int oppornent;

  /// 記譜を記録
  String boardHistory = "";

  /// 盤面のインデックスから記譜用の文字列に変換するテーブル
  final List<List> boardStrMap = [
    [Null, Null, Null, Null, Null, Null, Null, Null, Null, Null],
    [Null, "A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1", Null],
    [Null, "A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2", Null],
    [Null, "A3", "B3", "C3", "D3", "E3", "F3", "G3", "H3", Null],
    [Null, "A4", "B4", "C4", "D4", "E4", "F4", "G4", "H4", Null],
    [Null, "A5", "B5", "C5", "D5", "E5", "F5", "G5", "H5", Null],
    [Null, "A6", "B6", "C6", "D6", "E6", "F6", "G6", "H6", Null],
    [Null, "A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7", Null],
    [Null, "A8", "B8", "C8", "D8", "E8", "F8", "G8", "H8", Null],
    [Null, Null, Null, Null, Null, Null, Null, Null, Null, Null],
  ];

  /// 盤面及びゲームの情報を設定するコンストラクタ
  OthelloLogic(int myself) {
    this.board = [
      [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
      [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
      [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
      [-1, 0, 0, 0, 3, 0, 0, 0, 0, -1],
      [-1, 0, 0, 3, 1, 2, 0, 0, 0, -1],
      [-1, 0, 0, 0, 2, 1, 3, 0, 0, -1],
      [-1, 0, 0, 0, 0, 3, 0, 0, 0, -1],
      [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
      [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
      [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
    ];
    this.turn = 2;
    this.nextTurn = 1;
    this.myself = myself;
    this.oppornent = 1;
    boardHistory = "";
  }
  OthelloLogic.gameSetting(int myself) : this.myself = myself;

  /// 現在の盤面を取得
  List<List> get getCurrentBoard => this.board;

  /// 現在の記譜を取得
  String get getBoardHistory => this.boardHistory;

  /// 現在の手番を取得
  /// １：白、２：黒
  int get getCurrentTurn => this.turn;

  /// 着手可能であれば盤面を更新
  bool updateBoard(int columnIndex, int rowIndex) {
    if (this.board[columnIndex][rowIndex] == 3) {
      /// 置いた石を盤面に反映
      this.board[columnIndex][rowIndex] = this.turn;

      /// 裏返る石を盤面に反映
      flipDisc(columnIndex, rowIndex, 1, 0);
      flipDisc(columnIndex, rowIndex, -1, 0);
      flipDisc(columnIndex, rowIndex, 1, 1);
      flipDisc(columnIndex, rowIndex, -1, 1);
      flipDisc(columnIndex, rowIndex, 1, -1);
      flipDisc(columnIndex, rowIndex, -1, -1);
      flipDisc(columnIndex, rowIndex, 0, 1);
      flipDisc(columnIndex, rowIndex, 0, -1);

      /// 記譜に記録
      boardHistory += boardStrMap[columnIndex][rowIndex];

      /// ターンを交代
      changeTurn();
      return true;
    } else {
      return false;
    }
  }

  /// 着手可能であれば盤面を更新
  /// TODO: changeTurnを使い、自分のターンだけ着手可能な場所をセットすると、updateBoardができなくなってしまう。
  ///       仮の実装として追加。
  bool updateBoardNoClear(int columnIndex, int rowIndex) {
    if (this.board[columnIndex][rowIndex] == 3) {
      /// 置いた石を盤面に反映
      this.board[columnIndex][rowIndex] = this.turn;

      /// 裏返る石を盤面に反映
      flipDisc(columnIndex, rowIndex, 1, 0);
      flipDisc(columnIndex, rowIndex, -1, 0);
      flipDisc(columnIndex, rowIndex, 1, 1);
      flipDisc(columnIndex, rowIndex, -1, 1);
      flipDisc(columnIndex, rowIndex, 1, -1);
      flipDisc(columnIndex, rowIndex, -1, -1);
      flipDisc(columnIndex, rowIndex, 0, 1);
      flipDisc(columnIndex, rowIndex, 0, -1);

      /// 記譜に記録
      boardHistory += boardStrMap[columnIndex][rowIndex];

      /// ターンを交代
      changeTurnNoClear();
      return true;
    } else {
      return false;
    }
  }

  /// 文字列の記譜から盤面を生成
  bool createBoardFromHistory(String historyStr) {
    // 下記のブログを参考に"A1"から"H8"までの文字列に分割
    // https://qiita.com/sensuikan1973/items/e618137b8052501190d0
    final exp = RegExp(r'([A-H]{1}[1-8]{1})');

    /// 文字列を"A1"から"H8"に分割し、リストに変換
    List<String> splitedHistoryStr = exp.allMatches(historyStr).map((match) => match.group(0)).toList();
    bool matchStr = true;

    /// 文字列を"A1"から"H8"に分割し、リストに変換
    List<String> splitedCurrentHistoryStr = exp.allMatches(boardHistory).map((match) => match.group(0)).toList();

    /// 現在の盤面の記譜と更新する記譜の差を確認
    /// 現在の盤面まで記譜が一致したら、途中から盤面を再生
    /// 一致しない場合は、最初から盤面を再生
    if (splitedCurrentHistoryStr.length <= splitedHistoryStr.length) {
      for (int i = 0; i < splitedCurrentHistoryStr.length; i++) {
        if (splitedCurrentHistoryStr[i] != splitedHistoryStr[i]) {
          matchStr = false;
        }
      }
    } else {
      matchStr = false;
    }

    if (matchStr) {
      /// 現在の盤面から記譜を再生
      if (splitedCurrentHistoryStr.length < splitedHistoryStr.length) {
        updateCanPut();
        for (int k = splitedCurrentHistoryStr.length; k < splitedHistoryStr.length; k++) {
          for (int i = 1; i <= 8; i++) {
            for (int j = 1; j <= 8; j++) {
              /// 記譜用の文字列から盤面のインデックスに変換
              if (boardStrMap[i][j] == splitedHistoryStr[k]) {
                /// 盤面を更新
                updateBoardNoClear(i, j);

                /// パスを確認
                if (checkPass()) {
                  changeTurnNoClear();
                  if (checkPass()) {
                    return false;
                  }
                }
                i = 9;
                j = 9;
              }
            }
          }
        }
      } else {
        if (checkPass()) {
          return false;
        }
      }
    } else {
      /// 最初から記譜を再生
      /// 記譜、ボードの状態、手番を初期化
      boardHistory = "";

      /// ゲームの設定を初期化
      this.board = [
        [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
        [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
        [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
        [-1, 0, 0, 0, 3, 0, 0, 0, 0, -1],
        [-1, 0, 0, 3, 1, 2, 0, 0, 0, -1],
        [-1, 0, 0, 0, 2, 1, 3, 0, 0, -1],
        [-1, 0, 0, 0, 0, 3, 0, 0, 0, -1],
        [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
        [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
      ];
      this.turn = 2;
      this.nextTurn = 1;

      for (String str in splitedHistoryStr) {
        for (int i = 1; i <= 8; i++) {
          for (int j = 1; j <= 8; j++) {
            /// 記譜用の文字列から盤面のインデックスに変換
            if (boardStrMap[i][j] == str) {
              /// 盤面を更新
              updateBoardNoClear(i, j);

              /// パスを確認
              if (checkPass()) {
                changeTurnNoClear();
                if (checkPass()) {
                  return false;
                }
              }
              i = 9;
              j = 9;
            }
          }
        }
      }
    }

    /// changeTurnNoClearでは、ターンにかかわらず、着手可能な場所が盤面に表示されてしまうため、
    /// 最後に正しい情報を反映
    if (this.turn == this.myself) {
      /// 着手可能な場所を盤面に表示
      updateCanPut();
    } else {
      clearCanPut();
    }
    return true;
  }

  /// 指定した場所からdy,dx方向に挟める石があるか判定
  /// TODO: 関数の役割がわかりにくいから直したい
  bool searchDyDx(int columnIndex, int rowIndex, int dy, int dx) {
    int searchIndexDx = rowIndex + dx;
    int searchIndexDy = columnIndex + dy;

    do {
      if (board[searchIndexDy][searchIndexDx] == this.nextTurn) {
        searchIndexDx += dx;
        searchIndexDy += dy;
      } else if (board[searchIndexDy][searchIndexDx] == this.turn) {
        return true;
      }
    } while (board[searchIndexDy][searchIndexDx] != -1 &&
        board[searchIndexDy][searchIndexDx] != 0 &&
        board[searchIndexDy][searchIndexDx] != 3);
    return false;
  }

  /// 指定した場所に石が置いた時、挟めるかどうかを判定
  /// 指定した場所から、上下左右斜めの８方向をチェック
  /// １方向に、挟める石があった時点で終了
  bool canPut(int columnIndex, int rowIndex) {
    if (this.board[columnIndex][rowIndex] == 0 || this.board[columnIndex][rowIndex] == 3) {
      if (this.board[columnIndex + 1][rowIndex] == this.nextTurn) {
        if (searchDyDx(columnIndex + 1, rowIndex, 1, 0)) {
          return true;
        }
      }
      if (this.board[columnIndex][rowIndex + 1] == this.nextTurn) {
        if (searchDyDx(columnIndex, rowIndex + 1, 0, 1)) {
          return true;
        }
      }
      if (this.board[columnIndex - 1][rowIndex] == this.nextTurn) {
        if (searchDyDx(columnIndex - 1, rowIndex, -1, 0)) {
          return true;
        }
      }
      if (this.board[columnIndex][rowIndex - 1] == this.nextTurn) {
        if (searchDyDx(columnIndex, rowIndex - 1, 0, -1)) {
          return true;
        }
      }
      if (this.board[columnIndex + 1][rowIndex + 1] == this.nextTurn) {
        if (searchDyDx(columnIndex + 1, rowIndex + 1, 1, 1)) {
          return true;
        }
      }
      if (this.board[columnIndex - 1][rowIndex + 1] == this.nextTurn) {
        if (searchDyDx(columnIndex - 1, rowIndex + 1, -1, 1)) {
          return true;
        }
      }
      if (this.board[columnIndex + 1][rowIndex - 1] == this.nextTurn) {
        if (searchDyDx(columnIndex + 1, rowIndex - 1, 1, -1)) {
          return true;
        }
      }
      if (this.board[columnIndex - 1][rowIndex - 1] == this.nextTurn) {
        if (searchDyDx(columnIndex - 1, rowIndex - 1, -1, -1)) {
          return true;
        }
      }
    }
    return false;
  }

  /// 指定した場所からdy,dx方向の挟んだ石を返す
  bool flipDisc(int columnIndex, int rowIndex, int dy, int dx) {
    int searchIndexDx = rowIndex + dx;
    int searchIndexDy = columnIndex + dy;

    /// 指定した場所の石の色が、次番の石の色であれば、dx,dyずらしサーチを継続
    /// 最終的に、現在の手番の石の色があれば、挟んだと判定し盤面を更新
    if (board[searchIndexDy][searchIndexDx] == this.nextTurn) {
      if (flipDisc(searchIndexDy, searchIndexDx, dy, dx)) {
        /// 盤面の更新
        board[searchIndexDy][searchIndexDx] = this.turn;
        return true;
      }
    } else if (board[searchIndexDy][searchIndexDx] == this.turn) {
      return true;
    } else {
      return false;
    }
    return false;
  }

  /// 着手可能な場所を盤面に反映
  /// 一手前の着手可能な場所が設定されているため、削除してから、
  /// 現在の着手可能な場所を設定する
  void updateCanPut() {
    for (int i = 1; i <= 8; i++) {
      for (int j = 1; j <= 8; j++) {
        /// 一手前の着手可能な場所を削除
        if (this.board[i][j] == 3) {
          this.board[i][j] = 0;
        }

        /// 現在の着手可能な場所を設定
        if (canPut(i, j)) {
          this.board[i][j] = 3;
        }
      }
    }
  }

  /// 着手可能な場所を盤面から削除
  void clearCanPut() {
    for (int i = 1; i <= 8; i++) {
      for (int j = 1; j <= 8; j++) {
        if (this.board[i][j] == 3) {
          this.board[i][j] = 0;
        }
      }
    }
  }

  /// 着手可能な場所があるか判定
  bool checkPass() {
    for (int i = 1; i <= 8; i++) {
      for (int j = 1; j <= 8; j++) {
        /// 着手可能な場所が１箇所でもあれば、
        /// パスではないと判定し、falseを返す
        if (canPut(i, j)) {
          return false;
        }
      }
    }
    return true;
  }

  /// 手番を変更
  void changeTurn() {
    int tmp;
    tmp = this.turn;
    this.turn = this.nextTurn;
    this.nextTurn = tmp;

    /// 自分のターンであるかチェックし、
    /// 自分のターンであれば、着手可能な場所を盤面に表示
    if (this.turn == this.myself) {
      /// 着手可能な場所を盤面に表示
      updateCanPut();
    } else {
      clearCanPut();
    }
  }

  /// 手番を変更
  /// ターンと自分の石の色にかかわらず、着手可能な場所を盤面に表示
  /// TODO: 着手可能な場所をセットしないと、updateBoardができなくなってしまう。
  ///       仮の実装として追加。
  void changeTurnNoClear() {
    int tmp;
    tmp = this.turn;
    this.turn = this.nextTurn;
    this.nextTurn = tmp;

    /// 着手可能な場所を盤面に表示
    updateCanPut();
  }

  /// 黒石の数をカウント
  /// TODO: othello_uiに再度、判定ロジックが入ってしまうので修正したい
  int countBlack() {
    int black = 0;
    int white = 0;
    int blank = 0;
    for (int i = 1; i <= 8; i++) {
      for (int j = 1; j <= 8; j++) {
        if (this.board[i][j] == 2) {
          black++;
        } else if (this.board[i][j] == 1) {
          white++;
        } else if (this.board[i][j] == 0) {
          blank++;
        }
      }
    }

    /// 勝った方が空きマスの分をもらう
    return black > white ? black + blank : black;
  }
}
