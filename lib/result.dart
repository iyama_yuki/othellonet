import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  int black;
  int white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Result')),
        body: Column(children: <Widget>[
          /// 対局結果を表示するセクション
          header,
          result()
        ]));
  }

  Result(int black, int white) {
    this.black = black;
    this.white = white;
  }

  Widget header = Text("対局結果");

  Widget result() {
    if (black > white) {
      return Text(black.toString() + "対" + white.toString() + "で黒の勝ちです。");
    } else if (black < white) {
      return Text(black.toString() + "対" + white.toString() + "で白の勝ちです。");
    } else {
      return Text("引き分けです");
    }
  }
}
